$(document).ready(function(){

const w= 1500;
const h = 1100;
const p= 100;

d3.json("https://cdn.rawgit.com/freeCodeCamp/testable-projects-fcc/a80ce8f9/src/data/tree_map/video-game-sales-data.json")
    .then(function(gameSalesData){
        
        //Initialize colors
        const colorScale = d3.scaleOrdinal([...d3.schemeCategory10, ...d3.schemeSet2 ])

        //xScale
        const xScale = d3.scaleLinear()
                .domain([1, 3])
                .range([300, 700]);
        
        const yScale = d3.scaleLinear()
                .domain([1, 6])
                .range([800, 1000]);

        //Construct hierarchy, treemap and nodes.
        const treemap = d3.treemap()
                .tile(d3.treemapResquarify)
                .size([w,h-400])
                .round(true)
                .padding(2);

        const hierarchy = d3.hierarchy(gameSalesData)
                .sum((d) => d.value)
                .sort(function(a, b) { return b.height - a.height || b.value - a.value; });        
        treemap(hierarchy);

        //Const create svg
        const svg = d3.select("#graph-container")
                .append("svg")
                .attr("width", w)
                .attr("height", h)

        //Construct Treemap
        const treemapContainer = svg.append("g")
                                    .attr("class", "treemap");

        const category = treemapContainer.selectAll("g")
                        .data(hierarchy.leaves())
                        .enter()
                        .append("g")
                        .attr("transform", (d) => "translate(" + d.x0 + "," + d.y0 +")") 
                        .attr("fill", (d) => "#457895")
       
        
        category.append("rect")
                .attr("width", (d) => d.x1 - d.x0)
                .attr("height", (d) => d.y1 - d.y0)
                .attr("class", "tile")
                .style("fill", (d) => colorScale(d.data.category) )
                .attr("data-name", (d) => d.data.name)
                .attr("data-category", (d) => d.data.category)
                .attr("data-value", (d) => d.data.value)
                    
        category.append("text")
                .selectAll("tspan")
                .data((d) => d.data.name.split(/(?=[A-Z][^A-Z])/g))
                .enter()
                .append("tspan")
                .text((d,i) => d)
                .attr("x", 5)
                .attr("y", (d,i) => 15 + i * 12 );

        //Construct Legend
        console.log(colorScale.domain())

        const legend = svg.append("g")
                        .attr("id", "legend");;

        const label = legend.selectAll("g")
                .data(colorScale.domain())
                .enter()
                .append("g")    

        label.append("rect")
                .attr("fill", "black")
                .attr("x", (d,i) => xScale(i%3 + 1))
                .attr("y", (d,i) => yScale(parseInt(i/3)))
                .attr("width", 20)
                .attr("height", 20)
                .attr("class", "legend-item")
                .style("fill", (d) => colorScale(d))

        label.append("text")
                .attr("x", (d,i) => xScale(i%3 + 1) + 30)
                .attr("y", (d,i) => yScale(parseInt(i/3)) +15)
                .style("color", "black")
                .text((d) => d)
                .style("font-weight", "bold")
        
        // Hover behavior
        const tooltip = $("#tooltip");

        $(".tile").hover(function(){
                tooltip.html("<p>Name: "+ $(this).attr("data-name") +"</p>" + 
                "<p>Category: "+ $(this).attr("data-category") +"</p>" +
                "<p>Value: " + $(this).attr("data-value") +"</p>");

                tooltip.css("top", $(this).position().top - 120);
                tooltip.css("left", $(this).position().left - 250);
                tooltip.css("visibility", "visible");
                tooltip.attr("data-value", $(this).attr("data-value"))
        }, function(){
                tooltip.css("visibility", "hidden");
        })
    })
});